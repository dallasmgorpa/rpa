﻿using HtmlAgilityPack;
using OfficeOpenXml;
using System;
using System.Activities;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;

namespace ExcelWebFileConverter
{
    public class ExcelConversion : CodeActivity
    {
        [Category("Input")]
        [RequiredArgument]
        public InArgument<string> InputFilePath { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var originalFileName = InputFilePath.Get(context);

            if (!string.IsNullOrEmpty(originalFileName) && File.Exists(originalFileName))
            {
                var inputFile = new FileInfo(originalFileName);
                var rawHtml = File.ReadAllText(inputFile.FullName);

                var document = new HtmlDocument();
                document.LoadHtml(rawHtml);

                var headers = document.DocumentNode.SelectNodes("//tr[1]/td");

                if (headers != null && headers.Count > 0)
                {
                    DataTable table = new DataTable();
                    foreach (HtmlNode header in headers)
                    {
                        table.Columns.Add(header.InnerText);
                    }

                    foreach (var row in document.DocumentNode.SelectNodes("//tr[position()>1]"))
                    {
                        table.Rows.Add(row.SelectNodes("td").Select(td => td.InnerText).ToArray());
                    }

                    var worksheetName = inputFile.Name.Replace(inputFile.Extension, string.Empty);
                    var exportFileName = worksheetName + ".xlsx";

                    if (File.Exists(exportFileName))
                    {
                        File.Delete(exportFileName);
                    }

                    using (var package = new ExcelPackage(new FileInfo(exportFileName)))
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(worksheetName);
                        worksheet.Cells["A1"].LoadFromDataTable(table, true);
                        package.Save();
                    }

                }
                else
                {
                    throw new Exception("ERROR: No rows were found in the file.");
                }

            }
            else
            {
                throw new Exception("ERROR: File name cannot be empty!");
            }
        }
    }
}
