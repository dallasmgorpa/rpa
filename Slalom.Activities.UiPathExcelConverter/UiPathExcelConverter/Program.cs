﻿using HtmlAgilityPack;
using OfficeOpenXml;
using System;
using System.Data;
using System.IO;
using System.Linq;

namespace UiPathExcelConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            string originalFileName = null;

            Console.WriteLine("Please enter the full path and name of the file to read: ");
            originalFileName =  Console.ReadLine();

            //originalFileName = @"C:\Users\danialm\Downloads\Sample.xls";

            if (!string.IsNullOrEmpty(originalFileName) && File.Exists(originalFileName))
            {
                var inputFile = new FileInfo(originalFileName);
                var rawHtml = File.ReadAllText(inputFile.FullName);

                var document = new HtmlDocument();
                document.LoadHtml(rawHtml);

                var headers = document.DocumentNode.SelectNodes("//tr[1]/td");

                if (headers != null && headers.Count > 0)
                {
                    DataTable table = new DataTable();
                    foreach (HtmlNode header in headers)
                    {
                        table.Columns.Add(header.InnerText);
                    }

                    foreach (var row in document.DocumentNode.SelectNodes("//tr[position()>1]"))
                    {
                        table.Rows.Add(row.SelectNodes("td").Select(td => td.InnerText).ToArray());
                    }

                    Console.WriteLine("Read {0} rows", table.Rows.Count);

                    var worksheetName = inputFile.Name.Replace(inputFile.Extension,  string.Empty);
                    var exportFileName = worksheetName + ".xlsx";
                    
                    if (File.Exists(exportFileName))
                    {
                        File.Delete(exportFileName);
                    }

                    using (var package = new ExcelPackage(new FileInfo(exportFileName)))
                    {
                        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add(worksheetName);
                        worksheet.Cells["A1"].LoadFromDataTable(table, true);
                        package.Save();
                    }
                    
                }
                else
                {
                    Console.WriteLine("ERROR: No rows were found in the file.");
                }

            }
            else
            {
                Console.WriteLine("ERROR: File name cannot be empty!");
            }
        }
    }
}
